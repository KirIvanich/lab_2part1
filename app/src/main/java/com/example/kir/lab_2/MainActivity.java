package com.example.kir.lab_2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Kir on 06.12.2015.
 */
public class MainActivity extends Activity {

    private EditText editText;
    private TextView textView;
    private Button button_set;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        editText=(EditText)findViewById(R.id.tex_field);
        textView=(TextView)findViewById(R.id.text_label);
        button_set=(Button)findViewById(R.id.button_set);

        button_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(editText.getText());
            }
        });
    }
}
